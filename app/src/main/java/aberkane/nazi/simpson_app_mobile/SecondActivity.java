package aberkane.nazi.simpson_app_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import aberkane.nazi.simpson_app_mobile.model.Personnage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Aberkane on 30/09/2019.
 */
public class SecondActivity extends AppCompatActivity {
    Personnage personnage;
    EditText editnom ;
    EditText editprenom ;
    EditText editage ;
    EditText editurlimage;
    ImageView imageView;
    Button updateButton;
    Button deleteButton;
    private Long id;

    Retrofit retrofit = NetworkClient.getRetrofitClient();
    ApiService apiService = retrofit.create(ApiService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        // recuperation des elements de l'activity
        editnom =  findViewById(R.id.edit_nom);
        editprenom = findViewById(R.id.edit_prenom);
        editage = findViewById(R.id.edit_age);
        imageView= findViewById(R.id.image_perso);
        updateButton= findViewById(R.id.update_btn);
        editurlimage=findViewById(R.id.edit_url_image);
        updateButton.setOnClickListener(new UpdateButton());

        deleteButton= findViewById(R.id.delete_btn);
        deleteButton.setOnClickListener(new DeleteButton());

        // recuperation de l'id
        Intent intent = getIntent();
         this.id =intent.getLongExtra("id",0);// recuperation des donnée
        // Appel de la fonction find by id
        Call<Personnage> call = apiService.findById(id);
        call.enqueue(new Callback<Personnage>() {
            @Override
            public void onResponse(Call<Personnage> call, Response<Personnage> response) {
                personnage= response.body();
                setPersonnageDetails(personnage);
            }

            @Override
            public void onFailure(Call<Personnage> call, Throwable t) {

            }
        });



    }
    class UpdateButton implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = getIntent();
            Long id =intent.getLongExtra("id",0);
            String newNom = editnom.getText().toString();
            String newPrenom =  editprenom.getText().toString();
            int newAge=0;
            if(!editage.getText().toString().isEmpty()){
                newAge = Integer.parseInt(editage.getText().toString());
            }
            String urlimage=editurlimage.getText().toString();
            Personnage personnage = new Personnage(null, newNom, newPrenom, newAge,urlimage);
            Call<Personnage> update = apiService.Update(id, personnage);
            update.enqueue(new Callback<Personnage>() {
                @Override
                public void onResponse(Call<Personnage> call, Response<Personnage> response) {

                    Toast toast = Toast.makeText(getApplicationContext(), "Modification réussi", Toast.LENGTH_SHORT);
                    toast.show();
                    closeActivity();
                }

                @Override
                public void onFailure(Call<Personnage> call, Throwable t) {
                }
            });
        }
    }

    class DeleteButton implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent intent = getIntent();
            Long id =intent.getLongExtra("id",0);
            Call<Void> delete = apiService.Delete(id);
            delete.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Suppresion réussi", Toast.LENGTH_SHORT);
                    toast.show();
                    closeActivity();
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {

                }
            });
        }
    }

    /**
     * Cette fonction permet d'affiicher les information des personnage dans les champs edit text
     * @param personnage
     */
    public void setPersonnageDetails(Personnage personnage){
        //Récupértion des detail du personnage
        String nom = personnage.getNom();
        String prenom = personnage.getPrenom();
        int age = personnage.getAge();
        String ageString= Integer.toString(age);
        String urlPersonnage = personnage.getUrlimage();
        //on met les donnes dans les champs
        editnom.setText(nom);
        editprenom.setText(prenom);
        editage.setText(ageString);
        editurlimage.setText(urlPersonnage);
        //On met l'image
        String url=personnage.getUrlimage();
        //condition si il n'y a pas d'url pour ce personnage
        if (url.isEmpty()) {
            imageView.setImageResource(android.R.drawable.ic_dialog_alert);
        } else{
            Picasso.get().load(url).error(android.R.drawable.ic_dialog_alert).placeholder(android.R.drawable.ic_dialog_alert).centerCrop().fit().into(imageView);
        }

    }
    private void closeActivity(){
        this.finish();
    }
}