package aberkane.nazi.simpson_app_mobile;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Aberkane on 18/10/2019.
 */
public class NetworkClient {

    public static final String BASE_URL = "https://microservice-simpson.herokuapp.com/";

    public static Retrofit retrofit;

    /*
    Cette fonction permet de recuperer un client retrofit (une instance)
    */

    public static Retrofit getRetrofitClient() {

        //If condition to ensure we don't create multiple retrofit instances in a single application
        if (retrofit == null) {

            //Defining the Retrofit using Builder
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL) //This is the only mandatory call on Builder object.
                    .addConverterFactory(GsonConverterFactory.create()) // Convertor library used to convert response into POJO
                    .build();
        }

        return retrofit;
    }

}