package aberkane.nazi.simpson_app_mobile.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aberkane on 30/09/2019.
 */
public class Personnage implements Parcelable {
    @SerializedName("id")
    Long id;
    @SerializedName("nom")
    String nom;
    @SerializedName("prenom")
    String prenom;
    @SerializedName("age")
    int age ;
    @SerializedName("urlimage")
    String urlimage;


    public Personnage(Long id, String nom, String prenom, int age,String urlimage) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.urlimage=urlimage;
    }

    public Personnage(Parcel parcel){
        id=parcel.readLong();
        nom = parcel.readString();
        prenom = parcel.readString();
        age = parcel.readInt();
        urlimage=parcel.readString();
    }

    public static final Creator<Personnage> CREATOR = new Creator<Personnage>() {
        @Override
        public Personnage createFromParcel(Parcel in) {
            return new Personnage(in);
        }

        @Override
        public Personnage[] newArray(int size) {
            return new Personnage[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getUrlimage() {
        return urlimage;
    }

    public void setUrlimage(String urlimage) {
        this.urlimage = urlimage;
    }

    @Override
    public int describeContents() {
        return hashCode();
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(nom);
        parcel.writeString(prenom);
        parcel.writeInt(age);
        parcel.writeString(urlimage);

    }
}
