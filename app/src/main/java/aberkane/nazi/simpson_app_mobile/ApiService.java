package aberkane.nazi.simpson_app_mobile;

import java.util.List;

import aberkane.nazi.simpson_app_mobile.model.Personnage;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Aberkane on 18/10/2019.
 */
public interface ApiService {
    @GET("/personnages")
    Call<List<Personnage>> listRepos();
    @GET("/personnages/{id}")
    Call<Personnage> findById(@Path("id") long personnageId);
    @PUT("/personnages/update/{id}")
    Call<Personnage> Update(@Path("id") long personnageId, @Body Personnage personnage);
    @DELETE("/personnages/{id}")
    Call<Void> Delete(@Path("id") long personnageId);
}
