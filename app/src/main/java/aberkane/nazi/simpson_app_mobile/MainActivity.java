package aberkane.nazi.simpson_app_mobile;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import aberkane.nazi.simpson_app_mobile.model.Personnage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {
    private ListView listview;
    private CustomAdapter myAdapter;
    List<Personnage> listpersonnage = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listview = findViewById(R.id.listview);
        // les personnages
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        ApiService apiService = retrofit.create(ApiService.class);

        afficherListePerso(apiService);

    }

    /**
     * Cette fonction permet d'afficher la liste des personnages
     * @param apiService
     */
    public void afficherListePerso(ApiService apiService){
        Call<List<Personnage>> call = apiService.listRepos();
        call.enqueue(new Callback<List<Personnage>>() {
            @Override
            public void onResponse(Call<List<Personnage>> call, Response<List<Personnage>> response) {

                listpersonnage = response.body();
                myAdapter = new CustomAdapter(getApplicationContext(),listpersonnage);
                listview.setAdapter(myAdapter);
            }

            @Override
            public void onFailure(Call<List<Personnage>> call, Throwable t) {

            }
        });
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final Personnage item = (Personnage) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("id", item.getId());
                startActivity(intent);
            }

        });
    }

    public class CustomAdapter extends ArrayAdapter<Personnage> {
        private Context mContext;
        private List<Personnage> personnageList = new ArrayList<>();


        public CustomAdapter(@NonNull Context context, List<Personnage> list) {
            super(context, 0, list);
            mContext = context;
            personnageList = list;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View listItem = convertView;
            if(listItem == null)
                listItem = LayoutInflater.from(mContext).inflate(R.layout.row_layout,parent,false);

            Personnage lepersonnage = personnageList.get(position);


            // recuperation de l'immage
            ImageView imageViewMiniature= (ImageView) listItem.findViewById(R.id.imageMini);

            // Element afficher dans chque ligne de la listeview
            TextView name = (TextView) listItem.findViewById(R.id.label);
            String urlimage =lepersonnage.getUrlimage();
            if (urlimage.isEmpty()) {
                imageViewMiniature.setImageResource(android.R.drawable.ic_dialog_alert);
            } else{
                Picasso.get().load(urlimage).error(android.R.drawable.ic_dialog_alert).placeholder(android.R.drawable.ic_dialog_alert).centerCrop().fit().into(imageViewMiniature);
            }
            name.setText(lepersonnage.getPrenom());


            return listItem;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        ApiService apiService = retrofit.create(ApiService.class);

        afficherListePerso(apiService);
    }
}
